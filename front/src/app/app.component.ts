import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front';
}

@Injectable()
export class Api {
  private apiUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) {

  }

  getRx(): Promise<Api[]> {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
    });

  return this.http.get(this.apiUrl, {headers: headers})
    .toPromise()
    .then((res: Response) => res.json())
    .catch(err => {
      return Promise.reject(err.json().error || 'Serveur Error')
    });
  }
}
