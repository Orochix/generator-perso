const express = require('express');
const api = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());

// Connection à la base de donnée
mongoose.connect('mongodb://localhost:27017/generator', { useNewUrlParser: true }, () => {
  console.log('on est connecté')
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Erreur lors de la connexion"));
db.once("open", () => {
  console.log("Connexion à la base OK");
});


// Schéma
let userSchema = mongoose.Schema(
  { /* pour definir avec quel type de donne on travaille*/
    name: String /* Chaine de caractere*/
  },
  {
    versionKey: false /* sinon il y a toujours 0 dans notre bd */
  }
);

let userModel = mongoose.model('users', userSchema); /* model = example */
api.post("/addUser", (req, res) => {
  let newUser = new userModel();
  newUser.name = req.body.name;
  
  console.log(newUser.name);

  newUser.save((err) => {
    if (err) {
      res.send(err);
    }
    res.send('Nein');
  })


});

api.get('/user', (req, res) => {
  console.log('fait le demande sur le page user')
  userModel.find({},
    (err, user) => {
      console.log('AAAAA');
      if (err) {
        res.send("Pas possible d'afficher les utilisateurs");
      }
      res.json(user);
    });
});

api.listen(8080, () => {
  console.log('API')
})














 